TP3 : Docker

I. Setup

🌞 Installez Docker

#installer yum-utils

`sudo yum install -y yum-utils`

```
Installed:
  yum-utils-4.0.21-3.el8.noarch

Complete!
```

# setup repertoire stable

`sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo`

```
Adding repo from: https://download.docker.com/linux/centos/docker-ce.repo
```

# Install Docker Engine (version plus récente)

`sudo yum install docker-ce docker-ce-cli containerd.io`

```
Installed:
  checkpolicy-2.9-1.el8.x86_64
  container-selinux-2:2.167.0-1.module+el8.5.0+710+4c471e88.noarch
  containerd.io-1.4.12-3.1.el8.x86_64
  docker-ce-3:20.10.11-3.el8.x86_64
  docker-ce-cli-1:20.10.11-3.el8.x86_64
  docker-ce-rootless-extras-20.10.11-3.el8.x86_64
  docker-scan-plugin-0.9.0-3.el8.x86_64
  fuse-common-3.2.1-12.el8.x86_64
  fuse-overlayfs-1.7.1-1.module+el8.5.0+710+4c471e88.x86_64
  fuse3-3.2.1-12.el8.x86_64
  fuse3-libs-3.2.1-12.el8.x86_64
  libcgroup-0.41-19.el8.x86_64
  libslirp-4.4.0-1.module+el8.5.0+710+4c471e88.x86_64
  policycoreutils-python-utils-2.9-16.el8.noarch
  python3-audit-3.0-0.17.20191104git1c2f876.el8.1.x86_64
  python3-libsemanage-2.9-6.el8.x86_64
  python3-policycoreutils-2.9-16.el8.noarch
  python3-setools-4.3.0-2.el8.x86_64
  slirp4netns-1.1.8-1.module+el8.5.0+710+4c471e88.x86_64

Complete!
```

#Start docker

`sudo systemctl start docker`

`sudo systemctl enable docker`


#Vérification que Docker est installé correctement 

`sudo docker run hello-world`

```
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
2db29710123e: Pull complete
Digest: sha256:cc15c5b292d8525effc0f89cb299f1804f3a725c8d05e158653a563f15e4f685
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```

🌞 Setup Docker

#ajoutez votre utilisateur au groupe docker

`sudo usermod -aG docker toto`

il faudra quitter puis réouvrir une session pour que cela prenne effet

#vérifiez avec un "docker info" que tout est ok

`docker info`

```
Client:
 Context:    default
 Debug Mode: false
 Plugins:
  app: Docker App (Docker Inc., v0.9.1-beta3)
  buildx: Build with BuildKit (Docker Inc., v0.6.3-docker)
  scan: Docker Scan (Docker Inc., v0.9.0)
```

II. Premiers pas

1. Conteneur NGINX

🌞 Lancez un conteneur NGINX

`docker pull nginx`

```
Using default tag: latest
latest: Pulling from library/nginx
e5ae68f74026: Pull complete
21e0df283cd6: Pull complete
ed835de16acd: Pull complete
881ff011f1c9: Pull complete
77700c52c969: Pull complete
44be98c0fab6: Pull complete
Digest: sha256:9522864dd661dcadfd9958f9e0de192a1fdda2c162a35668ab6ac42b465f0603
Status: Downloaded newer image for nginx:latest
docker.io/library/nginx:latest
```

#Créer un fichier conf.d

`sudo nano /home/toto/nginx.conf`

```
  GNU nano 2.9.8                                                                                              
events {
    multi_accept       on;
    worker_connections 65535;
}

http {
  server {
    listen 80;

    server_name vm1.tp3.b1;

    location / {
      root /var/www/html/cesi;
      index index.html;
    }
  }
}
```

#Créer un simple fichier html avec "coucou" dedans

`sudo nano /home/toto/index.html`

#utilisez l'image NGINX officielle rt partagez le port 8080 de l'hôte vers le port 80 du conteneur

`docker run -d -p 8080:80 -v /home/toto/index.html:/var/www/html/cesi/index.html -v /home/toto/nginx.conf:/etc/nginx/nginx.conf --name test1 nginx`

🌞 Vous devez accéder à votre page HTML, depuis votre navigateur

La page s'affiche sans problème !

🌞 Manipuler le conteneur qui tourne

#donnez les commandes que vous utilisez pour récupérer un terminal dans le conteneur

`docker exec -it test1 bash`

```
root@993aa14db300:/#
```

#déterminer l'adresse IP locale du conteneur avec une commande docker inspect

`docker inspect test1 | grep "IPAddress"`

```
"SecondaryIPAddresses": null,
            "IPAddress": "172.17.0.3",
                    "IPAddress": "172.17.0.3",
```

#Limiter la RAM à 128M dans un docker identique :

`docker run -d -m 128M -p 8080:80 -v /home/toto/index.html:/var/www/html/cesi/index.html -v /home/toto/nginx.conf:/etc/nginx/nginx.conf --name test2 nginx`

`docker stats`

```
CONTAINER ID   NAME      CPU %     MEM USAGE / LIMIT   MEM %     NET I/O     BLOCK I/O       PIDS
89f7dd80d887   test2     0.00%     28.5MiB / 128MiB    22.27%    976B / 0B   8.19kB / 41kB   2
```

2. Une vraie application

🌞 Créer un réseau docker

`docker network create wiki`

#vous pouvez le voir et l'inspecter

`docker network ls`

```
NETWORK ID     NAME      DRIVER    SCOPE
475e14c50f79   bridge    bridge    local
21dd88f163fb   host      host      local
a7da95b61e14   none      null      local
695467acf0d5   wiki      bridge    local
```

`docker network inspect wiki`


🌞 Lancez un conteneur MySQL

il devra :

les variables d'environnement servent pour la conf initiale :

un mot de passe root défini

un user créé

un mot de passe pour ce user

une base de données wiki



être dans le réseau wiki

être lancé en daemon

`docker run --name wiki_db --network wiki -e MYSQL_ROOT_PASSWORD=OKijuh12 -e MYSQL_DATABASE=wiki -e MYSQL_USER=wiki -e MYSQL_PASSWORD=meow -d mysql:latest`


assurez vous qu'il est fonctionnel en vous connectant à la base

d'abord récupérez un shell dans le conteur
puis une connexion locale à la base avec une commande mysql


`docker exec -it wiki_db bash`




