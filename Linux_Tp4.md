TP4 : Automatisation

🌞 Go vagrant up !

`vagrant up`

#puis vagrant status pour vérifier l'état de la VM

`vagrant status`

```
Current machine states:

default                   running (hyperv)

```

#vérifiez que vous pouvez vagrant ssh dans la VM

`vagrant ssh`

3. Repackage

🌞 Modifier le Vagrantfile (et/ou le setup.sh) pour de nouveau repackager une box par vous-mêmes :

dans le fichier setup.sh j'ai ajouté ```yum install -y ansible```

`vagrant up --provision`

`vagrant status`

`vagrant ssh`

Vim et ansible sont installés!

`vagrant package --output ~/vagrant/part3/centos-ansible.box`

`vagrant box add centos-ansible centos-ansible.box`

`vagrant box list`

```
centos-ansible (hyperv, 0)
centos-cesi    (hyperv, 0)
centos/7       (hyperv, 2004.01)

```

🌞 Test !

Dans un nouveau vagrantfile dans le dossier part3 :
```
Vagrant.configure("2")do|config|
  # Notre box custom
  config.vm.box="centos-ansible"
end
```

`vagrant up`

Vim et ansible sont installés!


4. Ajout d'un node


🌞 Ecrivez un Vagrantfile qui déploie 3 machines dans le même réseau


`nano vagrantfile`

```
Vagrant.configure("2") do |config|

  # Configuration commune à toutes les machines
  config.vm.box = "centos-ansible"

  # Ajoutez cette ligne afin d'accélérer le démarrage de la VM (si une erreur 'vbguest' est levé>
  # config.vbguest.auto_update = false
  # Désactive les updates auto qui peuvent ralentir le lancement de la machine
  config.vm.box_check_update = false
  # La ligne suivante permet de désactiver le montage d'un dossier partagé (ne marche pas tout l>
  config.vm.synced_folder ".", "/vagrant", disabled: true

  # Config une première VM "admin"
  config.vm.define "admin" do |admin|
    admin.vm.network "private_network", ip: "192.168.10.10"
  end

  # Config une première VM "node1"
  config.vm.define "node1" do |node1|
    node1.vm.network "private_network", ip: "192.168.10.11"
  end

  # Config une première VM "node2"
  config.vm.define "node2" do |node2|
    node2.vm.network "private_network", ip: "192.168.10.12"
  end

end

```

🌞 Mettez en place une connexion SSH sans mot de passe

dans la VM admin.tp4.cesi

`ssh-keygen -t rsa -b 4096`

`ssh-copy-id 172.25.201.121`

`ssh-copy-id 172.25.192.238`

🌞 Créez vous un répertoire de travail

`mkdir ansible`

🌞 Créez un playbook minimaliste nginx.yml :

`sudo vim /home/vagrant/ansible/nginx.yml`

```
---
- name: Install nginx
  hosts: cesi
  become: true

  tasks:
  - name: Add epel-release repo
    yum:
      name: epel-release
      state: present

  - name: Install nginx
    yum:
      name: nginx
      state: present

  - name: Insert Index Page
    template:
      src: index.html.j2
      dest: /usr/share/nginx/html/index.html

  - name: Start NGiNX
    service:
      name: nginx
      state: started

```

🌞 Et créez un inventaire hosts.ini :

`sudo vim /home/vagrant/ansible/hosts.ini`

```
[cesi]
172.25.201.121
172.25.192.238

```

🌞 Enfin, créez un fichier index.html.j2 :

`sudo vim /home/vagrant/ansible/index.html.j2`

```
Hello from {{ ansible_default_ipv4.address }}
```

🌞 Lancez le playbook !

`ansible-playbook -i hosts.ini nginx.yml`

```
PLAY [Install nginx] *****************************************************************************

TASK [Gathering Facts] ***************************************************************************
ok: [172.25.201.121]
ok: [172.25.192.238]

TASK [Add epel-release repo] *********************************************************************
ok: [172.25.201.121]
ok: [172.25.192.238]

TASK [Install nginx] *****************************************************************************
changed: [172.25.192.238]
changed: [172.25.201.121]

TASK [Insert Index Page] *************************************************************************
changed: [172.25.201.121]
changed: [172.25.192.238]

TASK [Start NGiNX] *******************************************************************************
changed: [172.25.201.121]
changed: [172.25.192.238]

PLAY RECAP ***************************************************************************************
172.25.192.238             : ok=5    changed=3    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
172.25.201.121             : ok=5    changed=3    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

2. Création de playbooks

Il y a trois playbooks à réaliser dans cette partie : maria.yml, users.yml et firewall.yml.

A. Déploiement base de données : MariaDB

🌞 Créez un playbook apache.yml qui :

installe MariaDB

lance MariaDB

ajoute une base de données

`sudo vim apache.yml`

```
---
- name: Install mariadb
  hosts: cesi
  become: true

  tasks:
  - name: install mariadb
    yum:
      name: mariadb
      state: present

  - name: Start mysql
    service:
      name: mysql
      state: started

  - name: Create a new database with name 'mydb'
    community.mysql.mysql_db:
      name: mydb
      state: present

```

🌞 Puis un playbook users.yml qui :

ajoute un utilisateur admin

lui déposer votre clé SSH publique afin de pouvoir s'y connecter

utilisez le module authorized_key

`sudo vim users.yml`

```

```

