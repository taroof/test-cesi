I. Setup base de données

1. Install MariaDB

#Installer MariaDB sur la machine db.tp2.cesi

`sudo dnf install mariadb-server`
```
Installed:
  mariadb-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64
  mariadb-backup-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64
  mariadb-common-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64
  mariadb-connector-c-3.1.11-2.el8_3.x86_64
  mariadb-connector-c-config-3.1.11-2.el8_3.noarch
  mariadb-errmsg-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64
  mariadb-gssapi-server-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64
  mariadb-server-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64
  mariadb-server-utils-3:10.3.28-1.module+el8.4.0+427+adf35707.x86_64
  perl-DBD-MySQL-4.046-3.module+el8.4.0+577+b8fe2d92.x86_64
  perl-DBI-1.641-3.module+el8.4.0+509+59a8d9b3.x86_64
  perl-Math-BigInt-1:1.9998.11-7.el8.noarch
  perl-Math-Complex-1.59-420.el8.noarch
  psmisc-23.1-5.el8.x86_64

Complete!
```


#lancez-le avec une commande

`sudo systemctl enable mariadb`
```
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
```
`sudo systemctl start mariadb`

`systemctl status mariadb`
```
Active: active (running) since Tue 2021-12-07 14:55:06 CET; 3s ago
```

#déterminez sur quel port la base de données écoute

`sudo ss -lutpn`
```
tcp     LISTEN   0        80                       *:3306                  *:*       users:(("mysqld",pid=3143,fd=21))
```

#déterminez le(s) processus lié(s) au service MariaDB

`ps -ef | grep mysqld`
```
mysql       3208       1  0 15:45 ?        00:00:00 /usr/libexec/mysqld --basedir=/usr
toto        3394    1960  0 15:46 pts/0    00:00:00 grep --color=auto mysqld
```
#Firewall : ouvrez le port utilisé par MySQL

`sudo firewall-cmd --add-port=3306/tcp --permanent`

`sudo firewall-cmd --reload`
```
success
```

2. Conf MariaDB

🌞 Configuration élémentaire de la base

`mysql_secure_installation`
```
NOTE: RUNNING ALL PARTS OF THIS SCRIPT IS RECOMMENDED FOR ALL MariaDB
      SERVERS IN PRODUCTION USE!  PLEASE READ EACH STEP CAREFULLY!

In order to log into MariaDB to secure it, we'll need the current
password for the root user.  If you've just installed MariaDB, and
you haven't set the root password yet, the password will be blank,
so you should just press enter here.

Enter current password for root (enter for none):
```
Set root password? [Y/n] y

Remove anonymous users? [Y/n] y

Disallow root login remotely? [Y/n] y

Remove test database and access to it? [Y/n] y

Reload privilege tables now? [Y/n] y

🌞 Préparation de la base en vue de l'utilisation par NextCloud

`sudo mysql -u root -p`
```
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 18
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
```

`CREATE USER 'nextcloud'@'10.2.1.11' IDENTIFIED BY 'meow';`
```
Query OK, 0 rows affected (0.000 sec)
```

`CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;`
```
Query OK, 1 row affected (0.000 sec)
```
`GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.2.1.11';`
```
Query OK, 0 rows affected (0.000 sec)
```
`FLUSH PRIVILEGES;`
```
Query OK, 0 rows affected (0.000 sec)
```

🌞 Installez sur la machine web.tp2.cesi la commande mysql

`sudo dnf provides mysql`
```
Last metadata expiration check: 3:28:21 ago on Tue 07 Dec 2021 12:44:59 PM CET.
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7
```

`sudo dnf install mysql -y`
```
Installed:
  mariadb-connector-c-config-3.1.11-2.el8_3.noarch                mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64
  mysql-common-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64

Complete!
```

🌞 Tester la connexion

`mysql -h 10.2.1.12 -u nextcloud -p nextcloud`
```
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 20
Server version: 5.5.5-10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
```
`mysql> show tables;`
```
Empty set (0.00 sec)
```

II. Setup Apache

1. Install Apache

A. Apache

🌞 Installer Apache sur la machine

`sudo dnf install httpd`

```
Installed:
  apr-1.6.3-12.el8.x86_64
  apr-util-1.6.1-6.el8.1.x86_64
  apr-util-bdb-1.6.1-6.el8.1.x86_64
  apr-util-openssl-1.6.1-6.el8.1.x86_64
  httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64
  httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch
  httpd-tools-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64
  mod_http2-1.15.7-3.module+el8.5.0+695+1fa8055e.x86_64
  rocky-logos-httpd-85.0-3.el8.noarch

Complete!
```

🌞 Analyse du service Apache

#lancez le service httpd et activez le au démarrage

`systemctl enable httpd`

`systemctl start httpd`

`systemctl status`

#isolez les processus liés au service
+
#déterminez sous quel utilisateur sont lancés les processus Apache

`sudo ps -ef | grep httpd`

```
root        2050       1  0 20:19 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2051    2050  0 20:19 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2052    2050  0 20:19 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2053    2050  0 20:19 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      2054    2050  0 20:19 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
toto        2604    1492  0 20:35 pts/0    00:00:00 grep --color=auto httpd
```

#déterminez sur quel port écoute Apache par défaut

`sudo ss -lutpn`

```
Netid       State        Recv-Q       Send-Q             Local Address:Port              Peer Address:Port       Process
tcp         LISTEN       0            128                      0.0.0.0:2048                   0.0.0.0:*           users:(("sshd",pid=887,fd=5))
tcp         LISTEN       0            128                            *:80                           *:*           users:(("httpd",pid=2054,fd=4),("httpd",pid=2053,fd=4),("httpd",pid=2052,fd=4),("httpd",pid=2050,fd=4))
tcp         LISTEN       0            128                         [::]:2048                      [::]:*           users:(("sshd",pid=887,fd=7))
```

🌞 Un premier test

#ouvrez le port d'Apache dans le firewall

`sudo firewall-cmd --list-all`

```
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: eth0 eth1
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 2048/tcp 80/tcp 8888/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
  ```
  
`sudo firewall-cmd --add-port=80/tcp --permanent`
```
Warning: ALREADY_ENABLED: 80:tcp
success
```

#testez, depuis votre PC, que vous pouvez accéder à la page d'accueil par défaut d'Apache

La page s'affiche sans problème :D

B. PHP

🌞 Installer PHP

# ajout des dépôts EPEL

`sudo dnf install epel-release`

```
  Installing       : epel-release-8-13.el8.noarch                                                                   1/1
  Running scriptlet: epel-release-8-13.el8.noarch                                                                   1/1
  Verifying        : epel-release-8-13.el8.noarch                                                                   1/1

Installed:
  epel-release-8-13.el8.noarch

Complete!
```

`sudo dnf update`

```
Upgraded:
  binutils-2.30-108.el8_5.1.x86_64                         bpftool-4.18.0-348.2.1.el8_5.x86_64
  kernel-tools-4.18.0-348.2.1.el8_5.x86_64                 kernel-tools-libs-4.18.0-348.2.1.el8_5.x86_64
  libgcc-8.5.0-4.el8_5.x86_64                              libgomp-8.5.0-4.el8_5.x86_64
  libipa_hbac-2.5.2-2.el8_5.1.x86_64                       libsss_autofs-2.5.2-2.el8_5.1.x86_64
  libsss_certmap-2.5.2-2.el8_5.1.x86_64                    libsss_idmap-2.5.2-2.el8_5.1.x86_64
  libsss_nss_idmap-2.5.2-2.el8_5.1.x86_64                  libsss_sudo-2.5.2-2.el8_5.1.x86_64
  libstdc++-8.5.0-4.el8_5.x86_64                           python3-perf-4.18.0-348.2.1.el8_5.x86_64
  python3-sssdconfig-2.5.2-2.el8_5.1.noarch                sssd-2.5.2-2.el8_5.1.x86_64
  sssd-ad-2.5.2-2.el8_5.1.x86_64                           sssd-client-2.5.2-2.el8_5.1.x86_64
  sssd-common-2.5.2-2.el8_5.1.x86_64                       sssd-common-pac-2.5.2-2.el8_5.1.x86_64
  sssd-ipa-2.5.2-2.el8_5.1.x86_64                          sssd-kcm-2.5.2-2.el8_5.1.x86_64
  sssd-krb5-2.5.2-2.el8_5.1.x86_64                         sssd-krb5-common-2.5.2-2.el8_5.1.x86_64
  sssd-ldap-2.5.2-2.el8_5.1.x86_64                         sssd-nfs-idmap-2.5.2-2.el8_5.1.x86_64
  sssd-proxy-2.5.2-2.el8_5.1.x86_64                        unzip-6.0-45.el8_4.x86_64
Installed:
  bind-libs-32:9.11.26-6.el8.x86_64                         bind-libs-lite-32:9.11.26-6.el8.x86_64
  bind-license-32:9.11.26-6.el8.noarch                      bind-utils-32:9.11.26-6.el8.x86_64
  fstrm-0.6.1-2.el8.x86_64                                  geolite2-city-20180605-1.el8.noarch
  geolite2-country-20180605-1.el8.noarch                    kernel-4.18.0-348.2.1.el8_5.x86_64
  kernel-core-4.18.0-348.2.1.el8_5.x86_64                   kernel-modules-4.18.0-348.2.1.el8_5.x86_64
  libmaxminddb-1.2.0-10.el8.x86_64                          protobuf-c-1.3.0-6.el8.x86_64
  python3-bind-32:9.11.26-6.el8.noarch                      python3-ply-3.9-9.el8.noarch

Complete!
```

# ajout des dépôts REMI

`sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm`

```
Downloading Packages:
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                1/1
  Installing       : remi-release-8.5-2.el8.remi.noarch                                                             1/1
  Verifying        : remi-release-8.5-2.el8.remi.noarch                                                             1/1

Installed:
  remi-release-8.5-2.el8.remi.noarch

Complete!
```

`sudo dnf module enable php:remi-7.4`

```
Remi's Modular repository for Enterprise Linux 8 - x86_64                               4.3 kB/s | 858  B     00:00
Remi's Modular repository for Enterprise Linux 8 - x86_64                               3.0 MB/s | 3.1 kB     00:00
Importing GPG key 0x5F11735A:
 Userid     : "Remi's RPM repository <remi@remirepo.net>"
 Fingerprint: 6B38 FEA7 231F 87F5 2B9C A9D8 5550 9759 5F11 735A
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-remi.el8
Is this ok [y/N]: y
Remi's Modular repository for Enterprise Linux 8 - x86_64                               4.3 MB/s | 946 kB     00:00
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                              7.0 kB/s | 858  B     00:00
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                              3.0 MB/s | 3.1 kB     00:00
Importing GPG key 0x5F11735A:
 Userid     : "Remi's RPM repository <remi@remirepo.net>"
 Fingerprint: 6B38 FEA7 231F 87F5 2B9C A9D8 5550 9759 5F11 735A
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-remi.el8
Is this ok [y/N]: y
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                              7.2 MB/s | 2.0 MB     00:00
Dependencies resolved.
========================================================================================================================
 Package                     Architecture               Version                       Repository                   Size
========================================================================================================================
Enabling module streams:
 php                                                    remi-7.4

Transaction Summary
========================================================================================================================

Is this ok [y/N]: y
Complete!
```

# install de PHP et de toutes les libs PHP requises par NextCloud

`sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp`

```
Installed:
  checkpolicy-2.9-1.el8.x86_64                           environment-modules-4.5.2-1.el8.x86_64
  libicu69-69.1-1.el8.remi.x86_64                        libsodium-1.0.18-2.el8.x86_64
  oniguruma5php-6.9.7.1-1.el8.remi.x86_64                php74-libzip-1.8.0-1.el8.remi.x86_64
  php74-php-7.4.26-1.el8.remi.x86_64                     php74-php-bcmath-7.4.26-1.el8.remi.x86_64
  php74-php-cli-7.4.26-1.el8.remi.x86_64                 php74-php-common-7.4.26-1.el8.remi.x86_64
  php74-php-fpm-7.4.26-1.el8.remi.x86_64                 php74-php-gd-7.4.26-1.el8.remi.x86_64
  php74-php-gmp-7.4.26-1.el8.remi.x86_64                 php74-php-intl-7.4.26-1.el8.remi.x86_64
  php74-php-json-7.4.26-1.el8.remi.x86_64                php74-php-mbstring-7.4.26-1.el8.remi.x86_64
  php74-php-mysqlnd-7.4.26-1.el8.remi.x86_64             php74-php-opcache-7.4.26-1.el8.remi.x86_64
  php74-php-pdo-7.4.26-1.el8.remi.x86_64                 php74-php-pecl-zip-1.20.0-1.el8.remi.x86_64
  php74-php-process-7.4.26-1.el8.remi.x86_64             php74-php-sodium-7.4.26-1.el8.remi.x86_64
  php74-php-xml-7.4.26-1.el8.remi.x86_64                 php74-runtime-1.0-3.el8.remi.x86_64
  policycoreutils-python-utils-2.9-16.el8.noarch         python3-audit-3.0-0.17.20191104git1c2f876.el8.1.x86_64
  python3-libsemanage-2.9-6.el8.x86_64                   python3-policycoreutils-2.9-16.el8.noarch
  python3-setools-4.3.0-2.el8.x86_64                     scl-utils-1:2.0.2-14.el8.x86_64
  tcl-1:8.6.8-2.el8.x86_64

Complete!
```

2. Conf Apache

🌞 Analyser la conf Apache

#mettez en évidence, dans le fichier de conf principal d'Apache, la ligne qui inclut tout ce qu'il y a dans le dossier de drop-in nommé conf.d/

`sudo cat httpd.conf | grep conf.d`

```
# Load config files in the "/etc/httpd/conf.d" directory, if any.
IncludeOptional conf.d/*.conf
```

🌞 Créer un VirtualHost qui accueillera NextCloud

`sudo nano /etc/httpd/conf.d/vhost.conf`

```
<VirtualHost *:80>
  # on précise ici le dossier qui contiendra le site : la racine Web
  DocumentRoot /var/www/nextcloud/html/  

  # ici le nom qui sera utilisé pour accéder à l'application
  ServerName  web.tp2.cesi  

  <Directory /var/www/nextcloud/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```

`sudo systemctl stop httpd.service`

`sudo systemctl start httpd.service`

🌞 Configurer la racine web

#creéz ce dossier /var/www/nextcloud/html/

`sudo mkdir /var/www/nextcloud/html/ -p`

#faites appartenir le dossier et son contenu à l'utilisateur qui lance Apache

`sudo chown apache /var/www/nextcloud/ -R`


🌞 Configurer PHP

`timedatectl`
 ```
                Local time: Tue 2021-12-07 22:21:10 CET
           Universal time: Tue 2021-12-07 21:21:10 UTC
                 RTC time: Tue 2021-12-07 21:21:02
                Time zone: Europe/Paris (CET, +0100)
System clock synchronized: no
              NTP service: inactive
          RTC in local TZ: no
```

#modifiez le fichier /etc/opt/remi/php74/php.ini

`sudo nano /etc/opt/remi/php74/php.ini`		  

`sudo cat /etc/opt/remi/php74/php.ini | grep ";date.timezone"`

```
;date.timezone ="Europe/Paris"
```


III. NextCloud

🌞 Récupérer Nextcloud

`cd`

#télécharger le fichier zip de nextcloud

`curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip`

```
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  148M  100  148M    0     0  33.8M      0  0:00:04  0:00:04 --:--:-- 33.8M
```

`ls`

```
nextcloud-21.0.1.zip
```

🌞 Ranger la chambre

#extraire le contenu de NextCloud (beh ui on a récup un .zip)

`sudo unzip nextcloud-21.0.1.zip -d /var/www/nextcloud/html/`

#déplacer tout le contenu dans la racine Web

`cd nextcloud`

`sudo mv ./* /var/www/nextcloud/html/`

`sudo chown apache /var/www/nextcloud/ -R`

`ls -al /var/www/nextcloud/html/`

```
total 120
drwxr-xr-x. 13 apache root   4096 Dec  7 23:22 .
drwxr-xr-x.  3 apache root     18 Dec  7 22:05 ..
drwxr-xr-x. 43 apache wheel  4096 Apr  8  2021 3rdparty
drwxr-xr-x. 47 apache wheel  4096 Apr  8  2021 apps
-rw-r--r--.  1 apache wheel 17900 Apr  8  2021 AUTHORS
drwxr-xr-x.  2 apache wheel    67 Apr  8  2021 config
-rw-r--r--.  1 apache wheel  3900 Apr  8  2021 console.php
-rw-r--r--.  1 apache wheel 34520 Apr  8  2021 COPYING
drwxr-xr-x. 22 apache wheel  4096 Apr  8  2021 core
-rw-r--r--.  1 apache wheel  5122 Apr  8  2021 cron.php
-rw-r--r--.  1 apache wheel   156 Apr  8  2021 index.html
-rw-r--r--.  1 apache wheel  2960 Apr  8  2021 index.php
drwxr-xr-x.  6 apache wheel   125 Apr  8  2021 lib
-rw-r--r--.  1 apache wheel   283 Apr  8  2021 occ
drwxr-xr-x.  2 apache wheel    23 Apr  8  2021 ocm-provider
drwxr-xr-x.  2 apache wheel    55 Apr  8  2021 ocs
drwxr-xr-x.  2 apache wheel    23 Apr  8  2021 ocs-provider
-rw-r--r--.  1 apache wheel  3144 Apr  8  2021 public.php
-rw-r--r--.  1 apache wheel  5341 Apr  8  2021 remote.php
drwxr-xr-x.  4 apache wheel   133 Apr  8  2021 resources
-rw-r--r--.  1 apache wheel    26 Apr  8  2021 robots.txt
-rw-r--r--.  1 apache wheel  2446 Apr  8  2021 status.php
drwxr-xr-x.  3 apache wheel    35 Apr  8  2021 themes
drwxr-xr-x.  2 apache wheel    43 Apr  8  2021 updater
-rw-r--r--.  1 apache wheel   382 Apr  8  2021 version.php
```

#supprimer l'archive

`sudo rm nextcloud-21.0.1.zip`


4. Test

🌞 Modifiez le fichier hosts de votre PC

#faites pointer le nom web.tp2.cesi vers l'IP 10.2.1.11

j'ouvre le fichier C:\Windows\System32\drivers\etc\hosts

J'ajout "10.2.1.11 web.tp2.cesi" dans le fichier


🌞 Tester l'accès à NextCloud et finaliser son install'

Dans mon navigateur, je tape http://web.tp2.cesi

La page s'affiche sans problème :D

---

Partie 2 : Sécurisation

1. Conf SSH

🌞 Modifier la conf du serveur SSH

`sudo nano /etc/ssh/sshd_config`

```
PermitRootLogin no
PasswordAuthentication no

# algorithms
KexAlgorithms curve25519-sha256@libssh.org,diffie-hellman-group-exchange-sha256
MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,umac-128@openssh.com
Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr
hostkeyalgorithms ecdsa-sha2-nistp256-cert-v01@openssh.com,ecdsa-sha2-nistp384-cert-v01@openssh.com,ecdsa-sha2-nistp521-cert-v01@openssh.com,ssh-ed25519-cert-v01@openssh.com,rsa-sha2-512-cert-v01@openssh.com,rsa-sha2-256-cert-v01@openssh.com,ssh-rsa-cert-v01@openssh.com,ecdsa-sha2-nistp384,ecdsa-sha2-nistp521,ssh-ed25519,rsa-sha2-512,rsa-sha2-256
```

2. Bonus : Fail2Ban

🌞 Installez et configurez fail2ban

`sudo systemctl start firewalld`

`sudo systemctl enable firewalld`

#Il faut installer EPEL repository mais il est déjà installé, sinon la commande est:

`sudo dnf install epel-release -y`

`sudo dnf install fail2ban fail2ban-firewalld -y`

```
Installed:
  esmtp-1.2-15.el8.x86_64                 fail2ban-0.11.2-1.el8.noarch          fail2ban-firewalld-0.11.2-1.el8.noarch
  fail2ban-sendmail-0.11.2-1.el8.noarch   fail2ban-server-0.11.2-1.el8.noarch   libesmtp-1.0.6-18.el8.x86_64
  liblockfile-1.14-1.el8.x86_64           python3-systemd-234-8.el8.x86_64

Complete!
```
`sudo systemctl start fail2ban`

`sudo systemctl enable fail2ban`

#Copier le fichier de conf Fail2ban

`sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local`

#modifier le fichier de conf

`sudo nano /etc/fail2ban/jail.local`

```
bantime = 1h
findtime = 1h
maxretry = 5
```

#puis, permettre Fail2Ban de travailler avec firewalld (au lieu de iptables)

`sudo mv /etc/fail2ban/jail.d/00-firewalld.conf /etc/fail2ban/jail.d/00-firewalld.local`

#Restart Fail2Ban

`sudo systemctl restart fail2ban`

Créér un jail SSH :

#création de conf jail

`sudo nano /etc/fail2ban/jail.d/sshd.local`

#coller la suite dans le fichier 

```
[sshd]
enabled = true
bantime = 1d
maxretry = 3
```

#Restart Fail2Ban

`sudo systemctl restart fail2ban`

Fail2ban est bien installé et configuré, pour liberer une adresse du jail :

`sudo fail2ban-client unban <ADDRESS>`


II. Serveur Web

Il faut créer une nouvelle machine proxy.tp2.cesi sur 10.2.1.13


🌞 Installer NGINX

`sudo dnf install nginx`

```
Last metadata expiration check: 21:29:54 ago on Tue 07 Dec 2021 12:44:59 PM CET.
Package nginx-1:1.14.1-9.module+el8.4.0+542+81547229.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!
```

🌞 Configurer NGINX comme reverse proxy

#création d'un nouveau fichier de conf 

`sudo nano /etc/nginx/conf.d/proxy.conf`

```
server {
    listen 80;
    server_name web.tp2.cesi;

    location / {
       proxy_pass http://10.2.1.11;
    }
}
```

#dans C:\Windows\System32\drivers\etc\hosts

```
10.2.1.13 web.tp2.cesi
```

2. HTTPS

🌞 Générer une clé et un certificat avec la commande suivante :

`openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 -keyout server.key -out server.crt`

```
Generating a RSA private key
......................................................................................................................++++
......................................................................................................................................................................++++
writing new private key to 'server.key'
-----
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [XX]:FR
State or Province Name (full name) []:Bordeaux
Locality Name (eg, city) [Default City]:Bodreaux
Organization Name (eg, company) [Default Company Ltd]:CESI
Organizational Unit Name (eg, section) []:RISR
Common Name (eg, your name or your server's hostname) []:web.tp2.cesi
Email Address []:
```

`ls`
```
server.crt  server.key
```


🌞 Allez, faut ranger la chambre



#déplacez au bon endroit et renommez la clé et le certificat

`sudo mv server.key  /etc/pki/tls/private/web.tp2.cesi.key`

`sudo mv server.crt /etc/pki/tls/certs/web.tp2.cesi.crt`


🌞 Affiner la conf de NGINX

#modifier le fichier proxy.conf en ajoutant le port 443 et les 2 clés:

`sudo nano /etc/nginx/conf.d/proxy.conf`

```
server {
    listen 443 ssl http2;
    server_name web.tp2.cesi;
        ssl_certificate         /etc/pki/tls/private/web.tp2.cesi.key;
        ssl_certificate_key     /etc/pki/tls/certs/web.tp2.cesi.crt;

    location / {
     proxy_pass http://10.2.1.11;
        proxy_http_version  1.1;
    proxy_cache_bypass  $http_upgrade;

    proxy_set_header Upgrade           $http_upgrade;
    proxy_set_header Connection        "upgrade";
    proxy_set_header Host              $host;
    proxy_set_header X-Real-IP         $remote_addr;
    proxy_set_header X-Forwarded-For   $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header X-Forwarded-Host  $host;
    proxy_set_header X-Forwarded-Port  $server_port;
    }
}
```

#ouverture du port 443 dans le firewall

`sudo firewall-cmd --add-port=443/tcp --permanent`

`sudo firewall-cmd --reload`


🌞 Test!

La page s'affiche en https!

---

Partie 3 : Maintien en condition opérationnelle

I. Monitoring

2. Setup Netdata

🌞 Installez Netdata


`bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)`

```
  ^
  |.-.   .-.   .-.   .-.   .-.   .  netdata              .-.   .-.   .-.   .-
  |   '-'   '-'   '-'   '-'   '-'   is installed now!  -'   '-'   '-'   '-'
  +----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+--->
```

🌞 Démarrez Netdata

`sudo firewall-cmd --add-port=19999/tcp --permanent`

`sudo firewall-cmd --reload`


II. Backup

🌞 Téléchargez Borg

`cd`

`curl -SLO https://github.com/borgbackup/borg/releases/download/1.1.17/borg-linux64`

`sudo cp borg-linux64 /usr/local/bin/borg`

`sudo chown root:root /usr/local/bin/borg`

`sudo chmod 755 /usr/local/bin/borg`


🌞 Jouer avec Borg


#Créez un dépôt

`sudo mkdir /srv/backup`

`sudo /usr/local/bin/borg init -e repokey /srv/backup/`






